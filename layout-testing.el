(defvar parameters
  '(window-parameters . ((no-other-window . t)
                         (no-delete-other-windows . t)
			 (no-delete . t))))
(defvar my/buffer-list-buffer (get-buffer-create "*Buffer List*")
  "Buffer to display a list of open buffers.")

(defun my/disable-mode-line-and-header-line ()
  "Disable the mode line and header line for the buffer named 'example.txt'."
  (interactive)
  (let ((buf (get-buffer "*Buffer List*")))
    (when buf
      (with-current-buffer buf
        (setq-local mode-line-format nil)
        (setq-local header-line-format nil))
      (message "Disabled mode line and header line for buffer Buffer List."))))


(defun my/display-buffers ()
  "Display a list of current open buffers in a side window."
  (interactive)
  (setq user-current-buffer (current-buffer))
  (let ((buffers (seq-filter #'buffer-file-name (buffer-list)))
        (window (display-buffer-in-side-window
                 my/buffer-list-buffer
                 `((side . left)
                   (slot . 0)
                   (window-width . 30)
                   (preserve-size . (t . nil))
                   ,@parameters))))
    (setq-local window-mode-line-format nil)
    (set-window-dedicated-p (selected-window) t)
    (with-current-buffer my/buffer-list-buffer
      (erase-buffer)
      (dolist (buf buffers)
        (let ((buf-name (buffer-name buf)))
					;(message (format "%s" buf-name))
          (if (eq user-current-buffer buf)
              (insert (propertize (format "%s\n" buf-name)
                                  'face '(:foreground "#77dd77" :background "white")))
            (insert (format "%s\n" buf-name))))))))
;; :TODO: :NOTE|Make current buffer an SVG
(run-with-idle-timer .25 t #'my/display-buffers)


